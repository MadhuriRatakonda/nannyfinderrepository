//
//  FirstViewController.swift
//  NannyFinder
//
//  Created by Madishetty,Sumesh on 10/12/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
//class that allow parent to post their jobs with few details
class ParentPostingViewController: UIViewController {
    var pickerData:[String] = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        // Do any additional setup after loading the view, typically from a nib.
        pickerData = [ "Arkansas","California","Minnesota","Missouri","NewYork", "Ohio","Tennesse","Texas"]
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var posts:[ParentPosts] = []
    var state:String = "Arkansas"
    @IBOutlet weak var locationPicker: UIPickerView!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var addressLine1TF: UITextField!
    @IBOutlet weak var addressLine2TF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var mobileTF: UITextField!
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        state = pickerData[row]
    }
    @IBAction func unwindBack(segue: UIStoryboardSegue,sender: AnyObject) {
        
    }
    @IBAction func nextPageBTN(sender: AnyObject) {
        if firstNameTF.text == "" || firstNameTF.text == nil{
            displayMessage("FIRST NAME CANNOT BE EMPTY")
        }
        else if lastNameTF.text == "" || lastNameTF.text == nil{
            displayMessage("LAST NAME CANNOT BE EMPTY")
        }
        else if addressLine1TF.text == "" || addressLine1TF.text == nil{
            displayMessage("ADDRESS CANNOT BE EMPTY")
        }
        else if emailTF.text == "" || emailTF.text == nil{
            displayMessage("EMAILID CANNOT BE EMPTY")
        }
        else if mobileTF.text == "" || mobileTF.text == nil{
            displayMessage("MOBILE NUMBER CANNOT BE EMPTY")
        }
        else{
            displayAlertControllerWithTitle("save and next", message: "Do you want to save and continue")
        }
    }
    //segue to send the details filled in first page to the second page where parents can post their jobs
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "parentDetailsPage2"{
            let details = segue.destinationViewController as! ParentPostingViewController2
            details.firstName = firstNameTF.text
            details.lastName = lastNameTF.text
            details.address = addressLine1TF.text! + addressLine2TF.text!
            details.email = emailTF.text
            details.mobile = mobileTF.text
            details.state = self.state
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            navigationItem.backBarButtonItem = backItem
        }
    }
    // Pass in a String and it will be displayed in an alert view
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
    // Pass in a String and it will be displayed in an alert view
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:  { action in self.performSegueWithIdentifier("parentDetailsPage2", sender: self) }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
}

