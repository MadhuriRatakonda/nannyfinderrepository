//
//  Parents.swift
//  NannyFinder
//
//  Created by Ankam,Neha on 10/25/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import Foundation
class Parents{
    var parentName:String
    var address:String
    var emailId:String
    var mobileNumber:String
    var jobDescription:String
    var qualification:String
    var wageRate:Double
    var hoursAvailable:Double
    var startDate:String
    init(parentName:String, address:String, emailId:String, mobileNumber:String, jobDescription:String, qualification:String, wageRate:Double, hoursAvailable:Double, startDate:String){
        self.parentName = parentName
        self.address = address
        self.emailId = emailId
        self.mobileNumber = mobileNumber
        self.jobDescription = jobDescription
        self.qualification = qualification
        self.wageRate = wageRate
        self.hoursAvailable = hoursAvailable
        self.startDate = startDate
    }
    
}