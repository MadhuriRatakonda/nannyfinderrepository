//
//  PasswordRecoveryViewController.swift
//  NannyFinder
//
//  Created by Ratakonda,Madhuri on 10/23/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
import Parse
import Bolts
//class that helps user to recover their passwords
class PasswordRecoveryViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var userEmailTF: UITextField!
    @IBAction func submitBTN(sender: AnyObject) {
        if userEmailTF.text == "" || userEmailTF.text == nil {
            displayMessage("EMAILID CANNOT BE EMPTY")
        }
        else {
            //sending email
            PFUser.requestPasswordResetForEmailInBackground(userEmailTF.text!){
                (success:Bool,error:NSError?) ->Void in
                if(success){
                    self.displayMessage("Email was sent successfully")
                }
                if(error != nil){
                    self.displayMessage("Error in email address")
                }
                
            }
            displayAlertControllerWithTitle("Mail Sent", message: "PASSWORD SENT TO THE REGISTERED EMAIL")
        }
        
    }
    @IBAction func cancelBTN(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // Pass in a String and it will be displayed in an alert view
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
    // Pass in a String and it will be displayed in an alert view
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
                                                                    message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:  { action in self.performSegueWithIdentifier("forgotPasswordToHome", sender: self) }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
 
}
