//
//  parentDeletingPostsViewController.swift
//  NannyFinder
//
//  Created by Ankam,Neha on 11/14/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//
import UIKit
import Parse
import Bolts
//class that allows parent-users to delete the posts
class parentDeletingPostsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBOutlet weak var jobsTV: UITableView!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var emailTF: UITextField!
    var jobs:[Jobs] = []
    //function to retrieve the posts made by the parent
    @IBAction func getPostsBTN(sender: AnyObject) {
        let query = PFQuery(className:"Jobs")
        if emailTF.text!.characters.count == 0 {
            displayMessage("Enter your email ID ")
        }
        else{
            query.whereKey("email", equalTo:emailTF.text!)
            
            query.findObjectsInBackgroundWithBlock {
                (objects: [PFObject]?, error: NSError?) -> Void in
                if error == nil {
                    self.jobs = objects as! [Jobs]
                    self.jobsTV.reloadData()
                }
                else {
                    self.displayAlertWithTitle("Oops", message: "\(error!) \(error!.userInfo)")
                }
            }
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobs.count
    }
    var job1:Jobs!
    //function to populate tableview with posts made by the parent
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("deletePostsIdentifier", forIndexPath: indexPath)
        job1 = jobs[indexPath.row]
        cell.textLabel!.text = job1.jobDesc
        return cell
    }
    var job2:Jobs!
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool{
        return true
    }
    //function to delete posts made by the parent
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            for job in jobs{
                let query = PFQuery(className: "Jobs")
                query.whereKey("objectId", equalTo: "\(String(jobs[indexPath.row].objectId!))")
                query.findObjectsInBackgroundWithBlock {
                    (objects, error) -> Void in
                    for object in objects! {
                        object.deleteEventually()
                        self.jobs.removeAll()
                        self.jobsTV.reloadData()
                        self.emailTF.text = " "
                        self.displayAlertWithTitle("Success!",
                            message:"Yours post was deleted successfully")
                    }
                }
            }
        }
    }
    // Pass in a String and it will be displayed in an alert view
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
    // Pass in a String and it will be displayed in an alert view
    func displayAlertWithTitle(title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
}