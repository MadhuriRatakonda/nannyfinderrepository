//
//  HomeViewController.swift
//  NannyFinder
//
//  Created by Ankam,Neha on 10/29/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
import Parse
import Bolts

//Defines Home Page of our application
class HomeViewController: UIViewController {
    
    var type = ""
    var users:[UserRegistration] = []
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.animationImages = [
            UIImage(named:"images/i1.jpg")!,
            UIImage(named:"images/i2.jpg")!,
            UIImage(named:"images/i3.jpg")!,
            UIImage(named:"images/i4.jpg")!,
            UIImage(named:"images/i5.jpg")!,
            UIImage(named:"images/i6.jpg")!
            
        ]
        imageView.animationDuration = 12
        imageView.startAnimating()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    override func viewWillAppear(animated: Bool) {
        userNameTF.text = ""
        passwordTF.text = ""
    }
    var userExists = true
    var user:String = " "
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    //when the user clicks on signIn button,first the fileds are validated
    @IBAction func signInBTN(sender: AnyObject) {
        if userNameTF.text == "" || userNameTF.text == nil {
            displayMessage("USERNAME CANNOT BE EMPTY")
        }
        else if passwordTF.text == "" || passwordTF.text == nil {
            displayMessage("PASSWORD CANNOT BE EMPTY")
        }
        else{
            //checking the user credentials in database
            let query = PFQuery(className:"UserRegistration")
            query.whereKey("email", equalTo:userNameTF.text!)
            query.findObjectsInBackgroundWithBlock {
                (objects: [PFObject]?, error: NSError?) -> Void in
                if error == nil {
                    self.users = objects as! [UserRegistration]
                    for user in self.users{
                        if self.passwordTF.text != user.password{
                            self.userExists = false
                            self.displayMessage("Username or password is wrong")
                        }
                    }
                }
                if objects!.count == 0{
                    self.userExists = false
                    self.displayMessage("No user exists")
                }
                if self.userExists == true{
                    self.displayAlertControllerWithTitle(" Success", message: "Login Successfull")
                }
            }
        }
    }
    
    @IBAction func unwindLogout(segue:UIStoryboardSegue, sender: AnyObject){
        
    }
   //function to display alerts
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
     //function to display alerts with a title
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:  { action in self.performSegueWithIdentifier("Login", sender: self) }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    
}
