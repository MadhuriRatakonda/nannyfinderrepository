//
//  UserRegistration.swift
//  NannyFinder
//
//  Created by Ankam,Neha on 11/16/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import Foundation
import Parse
import Bolts
//class that represents UserRegistration entity
class UserRegistration:PFObject, PFSubclassing{
    @NSManaged var email:String
    @NSManaged var password:String
    static func parseClassName() -> String {
        return "UserRegistration"
    }
    
    
}