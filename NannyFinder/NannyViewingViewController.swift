
//  NannyViewingViewController.swift
//  NannyFinder
//
//  Created by Ankam,Neha on 10/12/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
import Parse
import Bolts
//class that allows nanny-user to view the jobs posted by parent-users
class NannyViewingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    var jobs:[Jobs] = []
    @IBOutlet weak var jobsTV: UITableView!
    override func viewWillAppear(animated: Bool) {
        let query = PFQuery(className:"Jobs")
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                self.jobs = objects as! [Jobs]
                self.jobsTV.reloadData()
            }
            else {
                self.displayAlertWithTitle("Oops", message: "\(error!) \(error!.userInfo)")
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobs.count
    }
    var job1:Jobs!
    //populating table view with jobs posted by parent-users
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("jobsIdentifier", forIndexPath: indexPath)
        job1 = jobs[indexPath.row]
        cell.textLabel!.text = job1.jobDesc
        cell.detailTextLabel!.text = job1.state
        return cell
    }
    // Pass in a String and it will be displayed in an alert view
    func displayAlertWithTitle(title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    //segue that passes the job details
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "jobDetails"{
            var details = segue.destinationViewController as! NannyViewingJobDetailsViewController
            details.job = jobs[(jobsTV.indexPathForSelectedRow?.row)!]
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            navigationItem.backBarButtonItem = backItem
        }
        
    }
    
}
