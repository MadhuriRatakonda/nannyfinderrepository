//
//  ParentViewing2ViewController.swift
//  NannyFinder
//
//  Created by Ratakonda,Madhuri on 11/20/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
//class that allow parent-user to view nanny details
class ParentViewing2ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var nanny:NannyDetails!
    @IBOutlet weak var stateLBL: UILabel!
    @IBOutlet weak var emailLBL: UILabel!
    @IBOutlet weak var mobileLBL: UILabel!
    @IBOutlet weak var experienceLBL: UILabel!
    @IBOutlet weak var genderLBL: UILabel!
    @IBOutlet weak var addressLBL: UILabel!
    override func viewWillAppear(animated: Bool) {
        stateLBL.text = nanny.state
        emailLBL.text = nanny.email
        mobileLBL.text = nanny.mobileNum
        experienceLBL.text = nanny.experience
        genderLBL.text = nanny.gender
        addressLBL.text = nanny.address
        self.navigationItem.title = nanny.fullName
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */ 
}
