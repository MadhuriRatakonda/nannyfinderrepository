//
//  AppDelegate.swift
//  NannyFinder
//
//  Created by Madishetty,Sumesh on 10/12/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
import Parse
import Bolts
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        var navigationBarAppearance = UINavigationBar.appearance()
        navigationBarAppearance.barTintColor = UIColor.init(red: 0.496, green: 0.996, blue: 0.832, alpha: 1.0)
        navigationBarAppearance.tintColor = UIColor.init(red: 0.074, green: 0.363, blue: 0.996, alpha: 1.0)
        UITabBar.appearance().barTintColor = UIColor.init(red: 0.496, green: 0.996, blue: 0.832, alpha: 1.0)
        UITabBar.appearance().tintColor = UIColor.init(red: 0.074, green: 0.363, blue: 0.996, alpha: 1.0)
        let configuration = ParseClientConfiguration {
            $0.applicationId = "C2I9F9PXMU88yPKwomTJcXV85ldC04wCnGn8bz1n"
            $0.clientKey = "19vNo3ZhO8euWH3aBBMsVm2jDa4n9l6u0SumocLN"
            $0.server = "https://parseapi.back4app.com"
        }
        Parse.initializeWithConfiguration(configuration)
        UserRegistration.registerSubclass()
        NannyDetails.registerSubclass()
        ParentPosts.registerSubclass()
        Jobs.registerSubclass()
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

