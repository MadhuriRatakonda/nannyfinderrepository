//
//  Jobs.swift
//  NannyFinder
//
//  Created by Ratakonda,Madhuri on 11/30/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import Foundation
import Parse
import Bolts
//class that represents Jobs entity
class Jobs:PFObject, PFSubclassing{
    @NSManaged var firstName:String
    @NSManaged var lastName:String
    @NSManaged var address:String
    @NSManaged var email:String
    @NSManaged var mobile:String
    @NSManaged var state:String
    @NSManaged var jobDesc:String
    @NSManaged var qualification:String
    @NSManaged var wageRate:String
    @NSManaged var hoursAvailable:String
    @NSManaged var startDate:String
    static func parseClassName() -> String {
        return "Jobs"
    }
    
    
}