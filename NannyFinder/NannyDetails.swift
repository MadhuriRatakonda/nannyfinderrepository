//
//  NannyDetails.swift
//  NannyFinder
//
//  Created by Ratakonda,Madhuri on 11/17/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import Foundation
import Parse
import Bolts
//class that represents NannyDetails entity
class NannyDetails:PFObject, PFSubclassing{
    @NSManaged var fullName:String
    @NSManaged var email:String
    @NSManaged var mobileNum:String
    @NSManaged var experience:String
    @NSManaged var address:String
    @NSManaged var gender:String
    @NSManaged var state:String
    static func parseClassName() -> String {
        return "NannyDetails"
    }
    
    
}