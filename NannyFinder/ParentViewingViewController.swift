//
//  SecondViewController.swift
//  NannyFinder
//
//  Created by Madishetty,Sumesh on 10/12/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
import Parse
import Bolts
//class that allows parent-users to view the nannys
class ParentViewingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var location: [String] = [ "Arkansas","California","Minnesota","Missouri","NewYork", "Ohio","Tennesse","Texas"]
    var stateNannys:[NannyDetails] = []
    var name = ""
    @IBOutlet weak var nannysTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(animated: Bool) {
        let query = PFQuery(className:"NannyDetails")
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                self.stateNannys = objects as! [NannyDetails]
                self.nannysTV.reloadData()
            }
            else {
                self.displayAlertWithTitle("Oops", message: "\(error!) \(error!.userInfo)")
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stateNannys.count
    }
    var nanny1:NannyDetails!
    //populating table view with nanny details
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("babySitterIdentifier", forIndexPath: indexPath)
        nanny1 = stateNannys[indexPath.row]
        cell.textLabel!.text = nanny1.fullName
        cell.detailTextLabel!.text = nanny1.state
        return cell
    }
    // Pass in a String and it will be displayed in an alert view
    func displayAlertWithTitle(title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    //segue to pass the data to ParentViewing2Controller where all the details of nannny can be found
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "nannyDetails"{
            var details = segue.destinationViewController as! ParentViewing2ViewController
            details.nanny = stateNannys[(nannysTV.indexPathForSelectedRow?.row)!]
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            navigationItem.backBarButtonItem = backItem
        }
    }
    
}

