//
//  SignUpViewController.swift
//  NannyFinder
//
//  Created by Ratakonda,Madhuri on 10/23/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
import Parse
import Bolts
//class that allows the user to signUp
class SignUpViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    var users:[UserRegistration] = []
    var userExists = false
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBAction func registerBTN(sender: AnyObject) {
        if emailTF.text == "" || emailTF.text == nil {
            displayMessage("EMAILID CANNOT BE EMPTY")
        }
        else if passwordTF.text == "" || passwordTF.text == nil {
            displayMessage("PASSWORD CANNOT BE EMPTY")
        }
        else if confirmPasswordTF.text == "" || confirmPasswordTF.text == nil {
            displayMessage("PASSWORD CANNOT BE EMPTY")
        }
        else if passwordTF.text != confirmPasswordTF.text {
            displayMessage("PASSWORD AND CONFIRM PASSWORD FIELDS ARE NOT SIMILAR")
        }
        else{
            //saving the user data in database
            let query = PFQuery(className:"UserRegistration")
            query.findObjectsInBackgroundWithBlock {
                (objects: [PFObject]?, error: NSError?) -> Void in
                
                if error == nil {
                    self.users = objects as! [UserRegistration]
                    for user in self.users{
                        if user.email == self.emailTF.text{
                            self.userExists = true
                            self.displayMessage("Email already exists")
                        }
                    }
                    
                }
                if self.userExists == false{
                    let register = UserRegistration()
                    register.email = self.emailTF.text!
                    register.password = self.passwordTF.text!
                    register.saveInBackgroundWithBlock({ (success, error) -> Void in
                        if success {
                            self.displayAlertControllerWithTitle("Registered", message: "REGISTRATION SUCCESSFUL")
                        }
                        else {
                            print("error")
                        }
                    })
                    
                }
            }
        }
    }
    @IBAction func resetBTN(sender: AnyObject) {
        emailTF.text = nil
        passwordTF.text = nil
        confirmPasswordTF.text = nil
        
    }
    // Pass in a String and it will be displayed in an alert view
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
    // Pass in a String and it will be displayed in an alert view
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
                                                                    message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:  { action in self.performSegueWithIdentifier("registered", sender: self) }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    
    @IBAction func cancelBTN(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    
    
    
}
