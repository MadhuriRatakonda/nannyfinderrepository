//
//  NannyParentViewController.swift
//  NannyFinder
//
//  Created by Ratakonda,Madhuri on 11/13/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit

//class that allows user to navigate to parent or nanny
class NannyParentViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.animationImages = [
            UIImage(named:"images/i1.jpg")!,
            UIImage(named:"images/i2.jpg")!,
            UIImage(named:"images/i3.jpg")!,
            UIImage(named:"images/i4.jpg")!,
            UIImage(named:"images/i5.jpg")!,
            UIImage(named:"images/i6.jpg")!
        ]
        imageView.animationDuration = 12
        imageView.startAnimating()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
