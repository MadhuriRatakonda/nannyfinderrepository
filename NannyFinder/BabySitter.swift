//
//  BabySitter.swift
//  NannyFinder
//
//  Created by Ankam,Neha on 10/25/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import Foundation
class BabySitter {
    var name:String
    var emailId:String
    var mobileNumber:String
    var experience:Int
    init(name:String, emailId:String, mobileNumber:String, experience:Int){
        self.name = name
        self.emailId = emailId
        self.mobileNumber = mobileNumber
        self.experience = experience
    }
    
}