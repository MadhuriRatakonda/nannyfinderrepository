//
//  NannyPostingViewController.swift
//  NannyFinder
//
//  Created by Ankam,Neha on 10/25/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
import Parse
import Bolts
//class that allows nanny to post their details which can be viewed by parents
class NannyPostingJobsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var pickerData:[String] = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        // Do any additional setup after loading the view.
        pickerData = [ "Arkansas","California","Minnesota","Missouri","NewYork", "Ohio","Tennesse","Texas"]
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var nanny:[NannyDetails] = []
    var state:String = ""
    @IBOutlet weak var locationPicker: UIPickerView!
    @IBOutlet weak var fullNameTF: UITextField!
    @IBOutlet weak var address1TF: UITextField!
    @IBOutlet weak var emailIdTF: UITextField!
    @IBOutlet weak var mobileNumberTF: UITextField!
    @IBOutlet weak var experienceTF: UITextField!
    @IBOutlet weak var nannyPhotoIMV: UIImageView!
    @IBOutlet weak var genderSC: UISegmentedControl!
    var gender:String = "male"
    @IBAction func genderSC(sender: AnyObject) {
        if genderSC.selectedSegmentIndex == 0{
            gender = "male"
        }
        else {
            gender = "female"
        }
    }
    //when user clicks on Add Details Button fields will be validated and then saved in database
    @IBAction func addDetailsBTN(sender: AnyObject) {
        if fullNameTF.text == "" || fullNameTF.text == nil{
            displayMessage("NAME CANNOT BE EMPTY")
        }
        else if address1TF.text == "" || address1TF.text == nil{
            displayMessage("ADDRESS CANNOT BE EMPTY")
        }
        else if emailIdTF.text == "" || emailIdTF.text == nil{
            displayMessage("EMAILID CANNOT BE EMPTY")
        }
        else if mobileNumberTF.text == "" || mobileNumberTF.text == nil{
            displayMessage("MOBILE NUMBER CANNOT BE EMPTY")
        }
        else if experienceTF.text == "" || experienceTF.text == nil{
            displayMessage("EXPERIENCE CANNOT BE EMPTY")
        }
        else{
            //saving details entered by nanny-user in database
            let query = PFQuery(className:"NannyDetails")
            query.findObjectsInBackgroundWithBlock {
                (objects: [PFObject]?, error: NSError?) -> Void in
                
                if error == nil {
                    self.nanny = objects as! [NannyDetails]
                    let nanny1 = NannyDetails()
                    nanny1.fullName = self.fullNameTF.text!
                    nanny1.email = self.emailIdTF.text!
                    nanny1.mobileNum = self.mobileNumberTF.text!
                    nanny1.experience = self.experienceTF.text!
                    nanny1.address = self.address1TF.text!
                    nanny1.gender = self.gender
                    nanny1.state = self.state
                    nanny1.saveInBackgroundWithBlock({ (success, error) -> Void in
                        if success {
                            self.displayMessage("Added your details successfully")
                        }
                        else {
                            print("error")
                        }
                    })
                    
                }
            }
            
            
        }
    }
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        state = pickerData[row]
    }
    // Pass in a String and it will be displayed in an alert view
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
    // Pass in a String and it will be displayed in an alert view
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:  { action in self.performSegueWithIdentifier("nannytab1logout", sender: self) }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
    }
    @IBAction func uploadPhoto(sender: AnyObject) { 
        var myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(myPickerController, animated: true, completion: nil)
        
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        nannyPhotoIMV.image = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
