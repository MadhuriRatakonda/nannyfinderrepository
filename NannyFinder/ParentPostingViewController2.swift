//
//  ParentPostingViewController2.swift
//  NannyFinder
//
//  Created by Ankam,Neha on 11/7/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
import Parse
import Bolts
//class that allows parent-users to post their jobs successfully
class ParentPostingViewController2: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var firstName : String!
    var lastName : String!
    var address : String!
    var mobile : String!
    var email : String!
    var state : String!
    var job:[Jobs] = []
    var date:NSDate!
    var startDate:String = " "
    @IBOutlet weak var descriptionTF: UITextField!
    @IBOutlet weak var qualificationTF: UITextField!
    @IBOutlet weak var hoursAvailableTF: UITextField!
    @IBOutlet weak var startDateDP: UIDatePicker!
    @IBOutlet weak var wageRateLBL: UILabel!
    @IBOutlet weak var wageSlider: UISlider!
    @IBAction func dateChanged(sender: AnyObject) {
        date =  self.startDateDP.date
        startDate = String(date)
    }
    @IBAction func wageRateSlider(sender: AnyObject) {
        var currentValue = wageSlider.value
        
        wageRateLBL.text = String(format: "%.2f", (currentValue)) + "$"
    }
    @IBAction func postBTN(sender: AnyObject) {
        
        if descriptionTF.text == "" || descriptionTF.text == nil{
            displayMessage("DESCRIPTION CANNOT BE EMPTY")
        }
        if qualificationTF.text == "" || qualificationTF.text == nil{
            displayMessage("QUALIFICATION CANNOT BE EMPTY")
        }
        if hoursAvailableTF.text == "" || hoursAvailableTF.text == nil{
            displayMessage("HOURS AVAILABLE CANNOT BE EMPTY")
        }
        else{
            //adding jobs with given details
            let query = PFQuery(className:"Jobs")
            query.findObjectsInBackgroundWithBlock {
                (objects: [PFObject]?, error: NSError?) -> Void in
                if error == nil {
                    self.job = objects as! [Jobs]
                    let job1 = Jobs()
                    job1.firstName = self.firstName
                    job1.lastName = self.lastName
                    job1.address = self.address
                    job1.email = self.email
                    job1.mobile = self.mobile
                    job1.state = self.state
                    job1.jobDesc = self.descriptionTF.text!
                    job1.qualification = self.qualificationTF.text!
                    job1.wageRate = String(self.wageRateLBL.text!)
                    job1.hoursAvailable = self.hoursAvailableTF.text!
                    job1.startDate = self.startDate
                    job1.saveInBackgroundWithBlock({ (success, error) -> Void in
                        if success {
                            self.displayMessage("Job created successfully")
                        }
                        else {
                            print("error")
                        }
                    })
                    
                }
            }
        }
    }
    @IBAction func logoutBTN(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    // Pass in a String and it will be displayed in an alert view
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
}
