//
//  parentPost.swift
//  NannyFinder
//
//  Created by Ratakonda,Madhuri on 11/17/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import Foundation
import Parse
import Bolts
//class that represents ParentPosts entity
class ParentPosts:PFObject, PFSubclassing{
    @NSManaged var firstName:String
    @NSManaged var lastName:String
    @NSManaged var address1:String
    @NSManaged var address2:String
    @NSManaged var emailId:String
    @NSManaged var mobileNum:String
    @NSManaged var state:String
    @NSManaged var jobDescription:String
    @NSManaged var qualification:String
    @NSManaged var wageRate:String
    @NSManaged var hoursAvailable:String
    @NSManaged var startDate:String
    static func parseClassName() -> String {
        return "ParentPosts"
    }
    
    
}