//
//  NannyViewingJobDetailsViewController.swift
//  NannyFinder
//
//  Created by Ratakonda,Madhuri on 12/1/16.
//  Copyright © 2016 Madishetty,Sumesh. All rights reserved.
//

import UIKit
//class that allows nanny-user to view the job details posted by parents
class NannyViewingJobDetailsViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var job:Jobs!
    @IBOutlet weak var fullNameLBL: UILabel!
    @IBOutlet weak var addressLBL: UILabel!
    @IBOutlet weak var emailLBL: UILabel!
    @IBOutlet weak var mobileLBL: UILabel!
    @IBOutlet weak var stateLBL: UILabel!
    @IBOutlet weak var jobDescLBL: UILabel!
    @IBOutlet weak var qualificationLBL: UILabel!
    @IBOutlet weak var wageRateLBL: UILabel!
    @IBOutlet weak var hrsAvailableLBL: UILabel!
    @IBOutlet weak var startDateLBL: UILabel!
    override func viewWillAppear(animated: Bool) {
        fullNameLBL.text = job.lastName+", "+job.firstName
        addressLBL.text = job.address
        emailLBL.text = job.email
        mobileLBL.text = job.mobile
        stateLBL.text = job.state
        jobDescLBL.text = job.jobDesc
        qualificationLBL.text = job.qualification
        wageRateLBL.text = job.wageRate
        hrsAvailableLBL.text = job.hoursAvailable
        startDateLBL.text = job.startDate
        self.navigationItem.title = job.lastName+", "+job.firstName
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
